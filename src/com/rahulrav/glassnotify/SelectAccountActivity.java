package com.rahulrav.glassnotify;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.regex.Pattern;

/**
 * The activity that can be used to select an account.
 */
public class SelectAccountActivity extends Activity {

  protected SharedPreferences preferences;

  private ProgressDialog progressDialog;

  private ListView accountList;

  private AccountsAdapter adapter;

  private GetRegisteredAccounts registeredAccountsTask;

  private ExecutorService executor;

  @Override
  public void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    preferences = getSharedPreferences(NotifierService.PREFERENCES_NAME, Context.MODE_PRIVATE);
    adapter = new AccountsAdapter(this, null, null);
    executor = ((GlassApplication)getApplication()).getExecutor();
    setContentView(R.layout.account_select);
    accountList = (ListView) findViewById(R.id.account_list);
    accountList.setAdapter(adapter);
    accountList.setOnItemClickListener(new AccountsAdapter.SelectAccountListener(this, adapter));
  }

  @Override
  public void onResume() {
    super.onResume();
    progressDialog = ProgressDialog.show(this, "Glass Notifier", "Loading Accounts", true);
    registeredAccountsTask = new GetRegisteredAccounts(this);
    if (Build.VERSION.SDK_INT < 11) {
      registeredAccountsTask.execute();
    } else {
      registeredAccountsTask.executeOnExecutor(executor);
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    if (registeredAccountsTask != null && !(registeredAccountsTask.getStatus() == AsyncTask.Status.FINISHED)) {
      registeredAccountsTask.cancel(true);
      registeredAccountsTask = null;
    }
  }

  public static class GetRegisteredAccounts extends AsyncTask<Void, Void, AccountsPlusPrimary> {

    private final SelectAccountActivity activity;

    public  GetRegisteredAccounts(final SelectAccountActivity activity) {
      this.activity = activity;
    }

    @Override
    protected AccountsPlusPrimary doInBackground(Void... params) {
      final AccountsPlusPrimary accountsPlusPrimary = new AccountsPlusPrimary();
      final Pattern emailPattern = Patterns.EMAIL_ADDRESS;
      final Account[] accounts = AccountManager.get(activity).getAccounts();
      if (accounts != null) {
        // de-dupe account names
        final Set<String> accountNames = new HashSet<String>(accounts.length);
        for(final Account account : accounts) {
          if (emailPattern.matcher(account.name).matches()) {
            accountNames.add(account.name);
          }
        }
        final List<String> listOfNames = new ArrayList<String>(accountNames.size());
        for (final String accountName : accountNames) {
          listOfNames.add(accountName);
        }
        accountsPlusPrimary.accountNames = listOfNames;
      }
      accountsPlusPrimary.primaryAccount = NotifierService.getPrimaryAccount(activity);
      return accountsPlusPrimary;
    }

    @Override
    protected void onPostExecute(final AccountsPlusPrimary accountsPlusPrimary) {
      activity.adapter.clear();
      // set primary account first
      activity.adapter.setPrimaryAccount(accountsPlusPrimary.primaryAccount);
      activity.adapter.addItems(accountsPlusPrimary.accountNames);
      activity.adapter.notifyDataSetChanged();
      activity.progressDialog.dismiss();
    }
  }

  static class AccountsPlusPrimary {
    /** The list of all registered accounts */
    public List<String> accountNames;

    /** The current selected primary account. */
    public String primaryAccount;
  }
}

package com.rahulrav.glassnotify;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.ExecutorService;

public class MainActivity extends Activity {
  /**
   * Status
   */
  TextView status;

  /**
   * Account task
   */
  PrimaryAccountTask accountTask;

  /**
   * Test notifications
   */
  Button notificationTest;

  /**
   * Setp whitelist.
   */
  Button setupWhiteList;

  /**
   * Setup primary account.
   */
  Button selectPrimaryAccount;

  /**
   * The exector service
   */
  ExecutorService executor;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    status = (TextView) findViewById(R.id.status);
    executor = ((GlassApplication)getApplication()).getExecutor();
    // attach listeners
    notificationTest = (Button) findViewById(R.id.notification_test);
    notificationTest.setOnClickListener(new NotificationTestHandler());
    setupWhiteList = (Button) findViewById(R.id.whitelist_setup);
    setupWhiteList.setOnClickListener(new WhitelistSetupHandler());
    selectPrimaryAccount = (Button) findViewById(R.id.account_setup);
    selectPrimaryAccount.setOnClickListener(new PrimaryAccountSetupHandler());
  }

  @Override
  protected void onResume() {
    super.onResume();
    accountTask = new PrimaryAccountTask(this, status);
    if (Build.VERSION.SDK_INT < 11) {
      accountTask.execute();
    } else {
      accountTask.executeOnExecutor(executor);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (accountTask != null && !(accountTask.getStatus() == AsyncTask.Status.FINISHED)) {
      accountTask.cancel(true);
      accountTask = null;
    }
  }

  /**
   * Displays the primary google account being used.
   */
  public static class PrimaryAccountTask extends AsyncTask<Void, Void, String> {
    private final Context context;
    private final TextView status;

    public PrimaryAccountTask(final Context context, final TextView status) {
      this.context = context;
      this.status = status;
    }

    @Override
    protected String doInBackground(final Void... contexts) {
      return NotifierService.getPrimaryAccount(context);
    }

    @Override
    protected void onPostExecute(final String emailAddress) {
      status.setText(String.format("Using primary account (%s)", emailAddress));
    }
  }

  /**
   * Triggers a test notification.
   */
  public static class NotificationTestHandler implements View.OnClickListener {
    @Override
    public void onClick(final View view) {
      final Context context = view.getContext();
      final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
          .setContentTitle("Glass Notifier")
          .setContentText("Thank you for using Glass Notifications.")
          .setSmallIcon(android.R.drawable.arrow_up_float);

      final NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
      manager.notify(0, builder.build());
    }
  }

  /**
   * Kicks off whitelist setup.
   */
  public static class WhitelistSetupHandler implements View.OnClickListener {
    @Override
    public void onClick(final View view) {
      final Context context = view.getContext();
      final Intent intent = new Intent();
      intent.setClass(context, WhitelistActivity.class);
      context.startActivity(intent);
    }
  }

  public static class PrimaryAccountSetupHandler implements View.OnClickListener {
    @Override
    public void onClick(final View view) {
      final Context context = view.getContext();
      final Intent intent = new Intent();
      intent.setClass(context, SelectAccountActivity.class);
      context.startActivity(intent);
    }
  }

}

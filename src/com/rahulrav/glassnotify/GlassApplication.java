package com.rahulrav.glassnotify;

import android.app.Application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The Application.
 */
public class GlassApplication extends Application {

  private ExecutorService executor;

  /** Return the Executor process for the application. */
  public ExecutorService getExecutor() {
    return executor;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    // use a standard cached thread pool
    executor = Executors.newCachedThreadPool();
  }
}

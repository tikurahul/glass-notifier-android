package com.rahulrav.glassnotify;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.rahulrav.glassnotify.util.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountsAdapter extends BaseAdapter {

  private final Context context;
  private final SharedPreferences preferences;
  private final LayoutInflater layoutInflator;
  private String primaryAccount;
  private AccountViewHolder currentSelectedViewHolder;

  protected List<String> accounts;

  public AccountsAdapter(final Context context, final String primaryAccount, final List<String> accounts) {
    this.context = context;
    this.primaryAccount = primaryAccount;
    this.accounts = new ArrayList<String>();
    currentSelectedViewHolder = null;
    layoutInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    preferences = context.getSharedPreferences(NotifierService.PREFERENCES_NAME, Context.MODE_PRIVATE);
    if (accounts != null) {
      this.accounts.addAll(accounts);
    }
  }

  public void setPrimaryAccount(final String primaryAccount) {
    this.primaryAccount = primaryAccount;
  }

  @Override
  public int getCount() {
    return accounts.size();
  }

  @Override
  public Object getItem(final int position) {
    return accounts.get(position);
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  @Override
  public long getItemId(final int position) {
    return position;
  }

  public void addItems(final Collection<String> accountNames) {
    if (accountNames == null) {
      return;
    }

    this.accounts.addAll(accountNames);
  }

  public void clear() {
    accounts.clear();
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    AccountViewHolder viewHolder = null;
    final String accountName = (String) getItem(position);
    if (convertView == null) {
      convertView = layoutInflator.inflate(R.layout.account_item, parent, false);
      viewHolder = new AccountViewHolder();
      viewHolder.account = (TextView) convertView.findViewById(R.id.account);
      viewHolder.accountSelected = (RadioButton) convertView.findViewById(R.id.account_is_selected);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (AccountViewHolder) convertView.getTag();
    }
    viewHolder.account.setText(accountName);
    final boolean isAccountSelected = accountName.equals(primaryAccount);
    if (isAccountSelected) {
      // keep track of the selected view
      currentSelectedViewHolder = viewHolder;
    }
    viewHolder.accountSelected.setChecked(isAccountSelected);
    return convertView;
  }

  public static class SelectAccountListener implements AdapterView.OnItemClickListener {

    private final SelectAccountActivity activity;
    private final AccountsAdapter adapter;

    public SelectAccountListener(final SelectAccountActivity activity, final AccountsAdapter adapter) {
      this.activity = activity;
      this.adapter = adapter;
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int position, final long id) {
      try {
        final String accountName = (String) adapterView.getItemAtPosition(position);
        final AccountViewHolder viewHolder = (AccountViewHolder) view.getTag();
        // flip the currentSelectedViewHolder
        if (adapter.currentSelectedViewHolder != null) {
          adapter.currentSelectedViewHolder.accountSelected.setChecked(false);
        }
        // update view holder
        adapter.currentSelectedViewHolder = viewHolder;
        // update the selected account
        viewHolder.accountSelected.setChecked(true);
        // update preferences
        final SharedPreferences.Editor editor = activity.preferences.edit();
        editor.putString(NotifierService.GOOGLE_ACCOUNT_PREF, accountName).apply();
      } catch(final Exception e) {
        Logger.e("Unable to update selected account", e);
      }
    }

  }
}

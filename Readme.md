# Glass-Notify-Android

The Android Client to the Glass Notification [service](https://bitbucket.org/tikurahul/glass-notify).

# Setup the client

* Go to [Glass Notification Service](http://glass-notify.appspot.com) and click on __Basic OAuth setup__ to complete OAuth setup.
* Once you complete the setup, click __Basic OAuth setup again__, to __verify__ if the setup was successful.
* Build and Install the APK (or get a copy of the [app](https://play.google.com/store/apps/details?id=com.rahulrav.glassnotify) from Google Play).
* Enable the Glass Notify Accessiblity Service (under Settings -> Accessibility).
* Launch the Glass Notify Application.
* Select the primary account that you want to use by clicking on __Setup Primary Account__. Make sure its the same email address (as the one you registered on http://glass-notify.appspot.com).
* Whitelist applications whose notifications you want to forward by clicking the __Setup Whitelist__ button.
* Click on the __Test Glass Notifier__ button.
* Your Android notification should have been forwarded to Google Glass.

# Step by step screenshots

![OAuth](https://bitbucket.org/tikurahul/glass-notifier-android/raw/37a2afa1e483706797c0bcf6eb350ad9ad81a6cf/images/oauth.png "Basic OAuth Step")
![Step-One](https://bitbucket.org/tikurahul/glass-notifier-android/raw/49da5fa8c8a81646a071dc5175faf5aed85e4148/images/setup-one.png "Step One")
![Step-Two](https://bitbucket.org/tikurahul/glass-notifier-android/raw/37a2afa1e483706797c0bcf6eb350ad9ad81a6cf/images/setup-two.png "Step Two")
![Setup Primary Account](https://bitbucket.org/tikurahul/glass-notifier-android/raw/35a10773e6af0883eaa79d2b46776c2050b6190c/images/account-select.png "Setup Primary Account")
![Quick Test](https://bitbucket.org/tikurahul/glass-notifier-android/raw/35a10773e6af0883eaa79d2b46776c2050b6190c/images/quick-test.png "Quick Test")
![Glass Notification](https://bitbucket.org/tikurahul/glass-notifier-android/raw/37a2afa1e483706797c0bcf6eb350ad9ad81a6cf/images/test-success.png "Glass Notification")
![Setup Whitelist](https://bitbucket.org/tikurahul/glass-notifier-android/raw/6c6eb61cc7041576b19c730cb2f946bf52331748/images/whitelist.png "Setup Whitelist")
